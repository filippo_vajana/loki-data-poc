from unittest import TestCase
from unittest.mock import patch, Mock
from loki_api import LokiAPI, LokiDown
import responses
import requests
import datetime
import time

class LokiAPITest(TestCase):
    @classmethod
    def setUpClass(cls):
        labels = {
            "label_1" : "l1",
            "label_2" : "l2"
        }
        cls.api = LokiAPI(logger_labels=labels)    
    

    @responses.activate
    def test_server_not_ready(self):
        responses.add(responses.GET, self.api.LOKI_STATUS_ENDPOINT,
                  json={}, status=500)
        
        self.assertRaises(
            LokiDown, 
            self.api.query, 
            start=datetime.datetime.now(), 
            end=datetime.datetime.now())

    def test_push_empty(self):
        self.assertRaises(ValueError, self.api.push)


    def test_query_100records(self):
        t_start = datetime.datetime.now()

        # prepare data
        i=100
        while i > 0:
            self.api.push(str(i))
            i = i - 1

        t_end = datetime.datetime.now()

        # query data        
        data = self.api.query(start=t_start, end=t_end)
        self.assertEqual(len(data), 100)