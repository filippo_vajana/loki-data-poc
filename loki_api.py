import logging
import logging_loki
import requests
import datetime
from typing import Dict, List, Tuple
from datetime import datetime as dtime
from datetime import timezone as tzone
import json
import time
import pprint
import pytz
from tqdm import tqdm

PUSH_TIME = []
QUERY_TIME = []

def push_time(func):        
        def wrapper(*args, **kargs):
            t_start = time.time()
            r = func(*args, **kargs)             
            t = (time.time() - t_start) * 1e3
            PUSH_TIME.append(t.__round__(1))
            return r
        return wrapper

def query_time(func):        
        def wrapper(*args, **kargs):
            t_start = time.time()
            r = func(*args, **kargs)             
            t = (time.time() - t_start) * 1e3
            QUERY_TIME.append(t)
            return r
        return wrapper

class LokiDown(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

class LokiAPI(object):
    LOKI_STATUS_ENDPOINT: str = '{}:{}/ready'
    LOKI_QUERY_ENDPOINT: str = '{}:{}/loki/api/v1/query_range'
    LOKI_QUERY_LABELS: Dict[str,str] = {}
    LOKI_QUERY_RECORDS_LIMIT = 4999
    LOKI_PUSH_ENDPOINT: str = '{}:{}/loki/api/v1/push'
    
    def __init__(
            self,
            logger_addr: str = 'http://localhost',
            logger_port: int = 3100,
            logger_name: str = 'loki-logger-dgx-test',
            logger_labels: Dict[str,str] = {}):

        # init constant
        self.LOKI_PUSH_ENDPOINT = self.LOKI_PUSH_ENDPOINT.format(logger_addr, logger_port)
        self.LOKI_STATUS_ENDPOINT = self.LOKI_STATUS_ENDPOINT.format(logger_addr, logger_port)
        self.LOKI_QUERY_ENDPOINT = self.LOKI_QUERY_ENDPOINT.format(logger_addr, logger_port)
        self.LOKI_QUERY_LABELS = logger_labels

        # connection optimization
        http_adapter = requests.adapters.HTTPAdapter(max_retries=0, pool_connections=100, pool_maxsize=100)
        self.http_session = requests.Session()
        self.http_session.mount('http://', http_adapter)

        # init loki handler
        handler = logging_loki.LokiHandler(
        url=self.LOKI_PUSH_ENDPOINT, 
        tags=self.LOKI_QUERY_LABELS,    
        version="1")

        # init logger object
        logger = logging.getLogger(logger_name)
        logger.setLevel(logging.INFO)
        logger.addHandler(handler)
        self.logger = logger


### UTILS METHODS
    @staticmethod
    def datetime_to_ns(dt: datetime.datetime) -> int:        
        timestamp = dt.timestamp()     
        ns = int(timestamp * 1e9)        
        return ns

    def check_server_ready(self) -> bool:
        r = requests.get(
            self.LOKI_STATUS_ENDPOINT)
        try:            
            r.raise_for_status()
            return True
        except requests.HTTPError as http_exc:
            print(http_exc)
            return False


### QUERY METHODS
    def __build_query_string(self) -> str:
        query = ""
        for k,v in self.LOKI_QUERY_LABELS.items():
            query = query + f'{k}="{v}",'
        
        query = query.rstrip(',')        
        return query
    
    def __get_data_in_range(self, start_ns: int, end_ns: int) -> requests.Response:
        query = self.__build_query_string()
        payload = {
            'query': '{{ {} }}'.format(query),
            'limit': self.LOKI_QUERY_RECORDS_LIMIT, # Max query items   
            'end': end_ns,
            'start': start_ns,
            'direction': 'forward'}

        r = requests.get(
            self.LOKI_QUERY_ENDPOINT,
            params=payload
        )

        try:            
            r.raise_for_status()
            return r
        except requests.HTTPError as http_exc:
            print(http_exc)
            return None
  

### PUBLIC API
    @push_time
    def push(self, data: str = '') -> None:
        if data == '':
            raise ValueError('invalid data')
        
        # build post query
        ts = LokiAPI.datetime_to_ns(dtime.now())
        values = [[f"{ts}", data]]        
        body = {
            "streams": 
            [
                {
                    "stream": self.LOKI_QUERY_LABELS,
                    "values": values
                }                
            ]
        }        
        
        header = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        r = self.http_session.post(url=self.LOKI_PUSH_ENDPOINT, json=body)        
    

    def __parse_data(self, response_data: str) -> List:
        # deserialize response
        r_json = json.loads(response_data)
        if r_json['status'] != 'success':
            raise Exception('query failed')

        log_data = r_json.get('data', {})
        log_results = log_data.get('result', [])
        if len(log_results) == 1:
            log_values = log_results[0].get('values', [])
            query_data = [(record[0], record[1]) for record in log_values]
            return query_data            
        else:
            return [] 


    @query_time
    def query(self, start: dtime, end: dtime) -> List[Tuple[int,str]]:
        # check if dtime is timezone aware
        if start.tzinfo is None or end.tzinfo is None:
            logging.warning('Timezone unaware times, please provide timezone aware datetime objects')
        
        # force utc if timezone aware        
        start = start.astimezone(pytz.utc)
        end = end.astimezone(pytz.utc)

        # check server status
        if self.check_server_ready() == False:
            raise LokiDown()
        
        # preparation
        start_ns = self.datetime_to_ns(start)
        end_ns = self.datetime_to_ns(end)
        result_data: List[Tuple[int,int]] = []      
        chunk_id = 0
        while True:    
            # get data
            r = self.__get_data_in_range(start_ns, end_ns)
            if r is None:
                raise ValueError('None response')

            # parse data
            tmp_data: List[Tuple[int,int]] = self.__parse_data(r.content)
            print(f'chunk {chunk_id} [{len(tmp_data)} records]')

            # append data
            result_data.extend(tmp_data)

            # check for query records limit
            if len(tmp_data) == self.LOKI_QUERY_RECORDS_LIMIT:
                # get the oldest ts for the next request
                last_ts = int(tmp_data[-1][0]) + 1
                start_ns = last_ts
                chunk_id += 1
            else:
                return result_data



if __name__ == "__main__":
    labels = {
            "label_1" : "L1",
            "label_2" : "L2"
        }
    api = LokiAPI(logger_labels=labels)

    # flush db
    # requests.post(f"http://localhost:3100/flush")

    # prepare data    
    t_start = datetime.datetime.now()
    for i in tqdm(range(0, 60000, 1), desc="Pushing data"):       
        api.push(str(i))
        i = i + 1    
    
    t_end = datetime.datetime.now()
    data = api.query(t_start, t_end)
    # pprint.pprint(data)
    pprint.pprint(len(data))

    # performance
    print(f"{sum(PUSH_TIME)//len(PUSH_TIME)} avg. ms/push")
    print(f"{sum(QUERY_TIME)//len(QUERY_TIME)} avg. ms/query")